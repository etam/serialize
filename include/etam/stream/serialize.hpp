//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_SERIALIZE_HPP
#define ETAM_STREAM_SERIALIZE_HPP

#include <ostream>

#include "typeName.hpp"


namespace etam {
namespace stream {

template <typename T>
struct _Serialize
{
    const T& v;
    bool printType;
};

template <typename T>
auto serialize(const T& v, bool printType = true)
{
    return _Serialize<T>{v, printType};
}


#define ETAM_SERIALIZE_BASIC(t) \
static inline \
std::ostream& operator<<(std::ostream& ostream, _Serialize<t> _serialize) \
{ \
    if (_serialize.printType) { \
        ostream << typeName<t>() << '{'; \
    } \
    ostream << _serialize.v; \
    if (_serialize.printType) { \
        ostream << '}'; \
    } \
    return ostream; \
}

ETAM_SERIALIZE_BASIC(bool)
ETAM_SERIALIZE_BASIC(char)
ETAM_SERIALIZE_BASIC(signed char)
ETAM_SERIALIZE_BASIC(unsigned char)
ETAM_SERIALIZE_BASIC(wchar_t)
ETAM_SERIALIZE_BASIC(char16_t)
ETAM_SERIALIZE_BASIC(char32_t)
ETAM_SERIALIZE_BASIC(short)
ETAM_SERIALIZE_BASIC(unsigned short)
ETAM_SERIALIZE_BASIC(int)
ETAM_SERIALIZE_BASIC(unsigned int)
ETAM_SERIALIZE_BASIC(long)
ETAM_SERIALIZE_BASIC(unsigned long)
ETAM_SERIALIZE_BASIC(long long)
ETAM_SERIALIZE_BASIC(unsigned long long)
ETAM_SERIALIZE_BASIC(float)
ETAM_SERIALIZE_BASIC(double)
ETAM_SERIALIZE_BASIC(long double)

#undef ETAM_SERIALIZE_BASIC


} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_SERIALIZE_HPP
