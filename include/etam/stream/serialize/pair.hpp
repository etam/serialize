//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_SERIALIZE_PAIR_HPP
#define ETAM_STREAM_SERIALIZE_PAIR_HPP

#include <utility>

#include "../serialize.hpp"
#include "../typeName/pair.hpp"


namespace etam {
namespace stream {

template <typename First, typename Second>
std::ostream& operator<<(std::ostream& ostream, _Serialize<std::pair<First,Second>> _serialize)
{
    if (_serialize.printType) {
        ostream << typeName<std::pair<First,Second>>();
    }
    ostream << '{' << serialize(_serialize.v.first, false) << ',' << serialize(_serialize.v.second, false) << '}';
    return ostream;
}


} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_SERIALIZE_PAIR_HPP
