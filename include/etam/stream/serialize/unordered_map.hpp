//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_SERIALIZE_UNORDERED_MAP_HPP
#define ETAM_STREAM_SERIALIZE_UNORDERED_MAP_HPP

#include <unordered_map>

#include "../serialize.hpp"
#include "../typeName/unordered_map.hpp"

#include "pair.hpp"


namespace etam {
namespace stream {

template <typename Key, typename Value>
std::ostream& operator<<(std::ostream& ostream, _Serialize<std::unordered_map<Key,Value>> _serialize)
{
    if (_serialize.printType) {
        ostream << typeName<std::unordered_map<Key,Value>>();
    }
    ostream << '{';
    for (const auto& i : _serialize.v) {
        ostream << serialize(i, false) << ',';
    }
    ostream << '}';
    return ostream;
}


} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_SERIALIZE_UNORDERED_MAP_HPP
