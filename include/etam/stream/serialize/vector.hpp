//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_SERIALIZE_VECTOR_HPP
#define ETAM_STREAM_SERIALIZE_VECTOR_HPP

#include <vector>

#include "../serialize.hpp"
#include "../typeName/vector.hpp"


namespace etam {
namespace stream {


template <typename T>
std::ostream& operator<<(std::ostream& ostream, _Serialize<std::vector<T>> _serialize)
{
    if (_serialize.printType) {
        ostream << typeName<std::vector<T>>();
    }
    ostream << '{';
    for (const auto& i : _serialize.v) {
        ostream << serialize(i, false) << ',';
    }
    ostream << '}';
    return ostream;
}


} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_SERIALIZE_VECTOR_HPP
