//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_HPP
#define ETAM_STREAM_TYPENAME_HPP

#include <ostream>


namespace etam {
namespace stream {

template <typename... T>
struct _TypeName {};

template <typename... T>
auto typeName()
{
    return _TypeName<T...>{};
}


template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<T*>)
{
    return ostream << typeName<T>() << '*';
}

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<T&>)
{
    return ostream << typeName<T>() << '&';
}

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<T&&>)
{
    return ostream << typeName<T>() << "&&";
}

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<const T>)
{
    return ostream << "const " << typeName<T>();
}

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<volatile T>)
{
    return ostream << "volatile " << typeName<T>();
}

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<const volatile T>)
{
    return ostream << "const volatile " << typeName<T>();
}


#define ETAM_TYPENAME_BASIC(t) \
static inline \
std::ostream& operator<<(std::ostream& ostream, _TypeName<t>) \
{ \
    return ostream << #t; \
}

ETAM_TYPENAME_BASIC(void)
ETAM_TYPENAME_BASIC(bool)
ETAM_TYPENAME_BASIC(char)
ETAM_TYPENAME_BASIC(signed char)
ETAM_TYPENAME_BASIC(unsigned char)
ETAM_TYPENAME_BASIC(wchar_t)
ETAM_TYPENAME_BASIC(char16_t)
ETAM_TYPENAME_BASIC(char32_t)
ETAM_TYPENAME_BASIC(short)
ETAM_TYPENAME_BASIC(unsigned short)
ETAM_TYPENAME_BASIC(int)
ETAM_TYPENAME_BASIC(unsigned int)
ETAM_TYPENAME_BASIC(long)
ETAM_TYPENAME_BASIC(unsigned long)
ETAM_TYPENAME_BASIC(long long)
ETAM_TYPENAME_BASIC(unsigned long long)
ETAM_TYPENAME_BASIC(float)
ETAM_TYPENAME_BASIC(double)
ETAM_TYPENAME_BASIC(long double)

#undef ETAM_TYPENAME_BASIC


} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_HPP
