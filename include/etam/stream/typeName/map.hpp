//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_MAP_HPP
#define ETAM_STREAM_TYPENAME_MAP_HPP

#include <map>

#include "../typeName.hpp"


namespace etam {
namespace stream {

template <typename Key, typename Value>
std::ostream& operator<<(std::ostream& ostream, _TypeName<std::map<Key,Value>>)
{
    return ostream << "std::map<" << typeName<Key>() << ',' << typeName<Value>() << '>';
}

} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_MAP_HPP
