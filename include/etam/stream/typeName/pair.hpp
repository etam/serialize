//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_PAIR_HPP
#define ETAM_STREAM_TYPENAME_PAIR_HPP

#include <utility>

#include "../typeName.hpp"


namespace etam {
namespace stream {

template <typename First, typename Second>
std::ostream& operator<<(std::ostream& ostream, _TypeName<std::pair<First,Second>>)
{
    return ostream << "std::pair<" << typeName<First>() << ',' << typeName<Second>() << '>';
}

} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_PAIR_HPP
