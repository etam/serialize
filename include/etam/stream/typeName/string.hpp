//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_STRING_HPP
#define ETAM_STREAM_TYPENAME_STRING_HPP

#include <string>

#include "../typeName.hpp"


namespace etam {
namespace stream {

static inline
std::ostream& operator<<(std::ostream& ostream, _TypeName<std::string>)
{
    return ostream << "std::string";
}

} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_STRING_HPP
