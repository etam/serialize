//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_UNORDERED_MAP_HPP
#define ETAM_STREAM_TYPENAME_UNORDERED_MAP_HPP

#include <unordered_map>

#include "../typeName.hpp"


namespace etam {
namespace stream {

template <typename Key, typename Value>
std::ostream& operator<<(std::ostream& ostream, _TypeName<std::unordered_map<Key,Value>>)
{
    return ostream << "std::unordered_map<" << typeName<Key>() << ',' << typeName<Value>() << '>';
}

} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_UNORDERED_MAP_HPP
