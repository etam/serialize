//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STREAM_TYPENAME_VECTOR_HPP
#define ETAM_STREAM_TYPENAME_VECTOR_HPP

#include <vector>

#include "../typeName.hpp"


namespace etam {
namespace stream {

template <typename T>
std::ostream& operator<<(std::ostream& ostream, _TypeName<std::vector<T>>)
{
    return ostream << "std::vector<" << typeName<T>() << '>';
}

} // namespace stream
} // namespace etam

#endif // ETAM_STREAM_TYPENAME_VECTOR_HPP
