//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STRING_TYPENAME_HPP
#define ETAM_STRING_TYPENAME_HPP

#include <string>


namespace etam {
namespace string {


template <typename T>
struct TypeName
{
    static
    std::string get();
};

template <typename T>
std::string typeName()
{
    return TypeName<T>::get();
}


template <typename T>
struct TypeName<T*>
{
    static
    std::string get()
    {
        return typeName<T>() + '*';
    }
};

template <typename T>
struct TypeName<T&>
{
    static
    std::string get()
    {
        return typeName<T>() + '&';
    }
};

template <typename T>
struct TypeName<T&&>
{
    static
    std::string get()
    {
        return typeName<T>() + "&&";
    }
};

template <typename T>
struct TypeName<const T>
{
    static
    std::string get()
    {
        using namespace std::string_literals;
        return "const "s + typeName<T>();
    }
};

template <typename T>
struct TypeName<volatile T>
{
    static
    std::string get()
    {
        using namespace std::string_literals;
        return "volatile "s + typeName<T>();
    }
};

#define ETAM_TYPENAME_BASIC(t) \
template <> \
struct TypeName<t> \
{ \
    static \
    std::string get() \
    { \
        return #t; \
    } \
};

ETAM_TYPENAME_BASIC(void)
ETAM_TYPENAME_BASIC(bool)
ETAM_TYPENAME_BASIC(char)
ETAM_TYPENAME_BASIC(signed char)
ETAM_TYPENAME_BASIC(unsigned char)
ETAM_TYPENAME_BASIC(wchar_t)
ETAM_TYPENAME_BASIC(char16_t)
ETAM_TYPENAME_BASIC(char32_t)
ETAM_TYPENAME_BASIC(short)
ETAM_TYPENAME_BASIC(unsigned short)
ETAM_TYPENAME_BASIC(int)
ETAM_TYPENAME_BASIC(unsigned int)
ETAM_TYPENAME_BASIC(long)
ETAM_TYPENAME_BASIC(unsigned long)
ETAM_TYPENAME_BASIC(long long)
ETAM_TYPENAME_BASIC(unsigned long long)
ETAM_TYPENAME_BASIC(float)
ETAM_TYPENAME_BASIC(double)
ETAM_TYPENAME_BASIC(long double)

#undef ETAM_TYPENAME_BASIC


} // namespace string
} // namespace etam

#endif // ETAM_STRING_TYPENAME_HPP
