//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STRING_TYPENAME_MAP_HPP
#define ETAM_STRING_TYPENAME_MAP_HPP

#include <map>

#include "../typeName.hpp"


namespace etam {
namespace string {

template <typename Key, typename Value>
struct TypeName<std::map<Key, Value>>
{
    static
    std::string get()
    {
        using namespace std::string_literals;
        return "std::map<"s + typeName<Key>() + ',' + typeName<Value>() + '>';
    }
};

} // namespace string
} // namespace etam

#endif // ETAM_STRING_TYPENAME_MAP_HPP
