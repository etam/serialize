//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STRING_TYPENAME_SET_HPP
#define ETAM_STRING_TYPENAME_SET_HPP

#include <set>

#include "../typeName.hpp"


namespace etam {
namespace string {

template <typename T>
struct TypeName<std::set<T>>
{
    static
    std::string get()
    {
        using namespace std::string_literals;
        return "std::set<"s + typeName<T>() + '>';
    }
};

} // namespace string
} // namespace etam

#endif // ETAM_STRING_TYPENAME_SET_HPP
