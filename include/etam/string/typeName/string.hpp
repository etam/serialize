//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STRING_TYPENAME_STRING_HPP
#define ETAM_STRING_TYPENAME_STRING_HPP

#include <string>

#include "../typeName.hpp"


namespace etam {
namespace string {

template <>
struct TypeName<std::string>
{
    static
    std::string get()
    {
        return "std::string";
    }
};

} // namespace string
} // namespace etam

#endif // ETAM_STRING_TYPENAME_STRING_HPP
