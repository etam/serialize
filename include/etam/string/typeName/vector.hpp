//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef ETAM_STRING_TYPENAME_VECTOR_HPP
#define ETAM_STRING_TYPENAME_VECTOR_HPP

#include <vector>

#include "../typeName.hpp"


namespace etam {
namespace string {

template <typename T>
struct TypeName<std::vector<T>>
{
    static
    std::string get()
    {
        using namespace std::string_literals;
        return "std::vector<"s + typeName<T>() + '>';
    }
};

} // namespace string
} // namespace etam

#endif // ETAM_STRING_TYPENAME_VECTOR_HPP
