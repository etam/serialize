//
//     Copyright Adam Mizerski <adam@mizerski.pl> 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <etam/stream/serialize.hpp>
#include <etam/stream/serialize/map.hpp>
#include <etam/stream/serialize/pair.hpp>
#include <etam/stream/serialize/set.hpp>
#include <etam/stream/serialize/string.hpp>
#include <etam/stream/serialize/unordered_map.hpp>
#include <etam/stream/serialize/vector.hpp>


int main()
{
    using etam::stream::serialize;
    std::cout << serialize(5) << '\n';
    std::cout << serialize(std::vector<int>{1,2,3,4}) << '\n';
    std::cout << serialize(std::pair<int, double>{7, 1.3}) << '\n';
    std::cout << serialize(std::map<std::string,std::set<int>>{{"a",{1,2,3}},{"b",{4,5,6}},{"c",{7,8,9}}}) << '\n';
    std::cout << serialize(std::unordered_map<std::string,std::set<int>>{{"a",{1,2,3}},{"b",{4,5,6}},{"c",{7,8,9}}}) << '\n';
}
